module.exports = app => {
  function existsOrError(value){
    if(!value) throw msg /* O valor nao estiver setado, manda msg */
    if(Array.isArray(value)&& value.length === 0) throw msg /* Se o valor estiver vazio, manda msg*/
    if(typeof value === 'string' && !value.trim()) throw msg /* String com espacos vazios, manda msg*/
  }
  
  function notExistsOrError(value, msg) {
    try{
      existsOrError(value, msg)
    } catch(msg) {
      return
    }
    throw msg
  }
  
  function equalsOrError(valueA, valueB, msg) {
    if(valueA !== valueB) throw msg /* Se o valor a for diferente do valor b, lanca msg */
  }

  return { existsOrError, notExistsOrError, equalsOrError}
}