const app = require('express')()
const consign = require('consign') /* Ajuda na dependecias(comunicacao) dos arquivos do projeto */
const db = require('./config/db')

app.db = db /* middleware de banco de dados */

consign()
  .then('./config/middlewares.js')
  .then('./api/validation.js')
  .then('./api')
  .then('./config/routes.js')
  .into(app)

app.listen(3000, () => {
  console.log('Backend executando...')
})